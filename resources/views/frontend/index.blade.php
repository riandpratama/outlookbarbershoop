<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Outlookbarbershop</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/logo.png')}}">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/gijgo.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/slicknav.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <!-- <link rel="stylesheet" href="css/responsive.css"> -->
</head>

<body>
    <!-- header-start -->
    <header>
        <div class="header-area ">
            <div id="sticky-header" class="main-header-area">
                <div class="container-fluid p-0">
                    <div class="row align-items-center no-gutters">
                            <div class="col-xl-3 col-lg-3">
                                <div class="logo-img">
                                    <a href="index.html">
                                        <img src="{{ asset('assets/img/logo.png') }}" alt="">
                                    </a>
                                </div>
                            </div>
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header-end -->

    <!-- slider_area_start -->
    <div class="slider_area">
        <div class="single_slider d-flex align-items-center justify-content-center slider_bg_1 overlay2">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="slider_text text-center">
                            <img src="{{ asset('assets/img/banner/barber_text.png') }}" alt="">
                            <h3>OUTLOOK <br>
                            <p>Professional Care</p>
                        </div>
                    </div>
                    <div class="col-xl-12 text-center">
                    <a class="boxed-btn3" href="https://outlookbooking.vercel.app">Booking Now</a>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider_area_end -->

    <!-- about_area_start -->
    <div class="about_area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-6 col-lg-6">
                    <div class="about_thumb">
                        <img src="{{ asset('assets/img/about/about_lft.jpg') }}" alt="">
                        <div class="opening_hour text-center">
                                <i class="flaticon-clock"></i>
                                <h3>Opening Hour</h3>
                                <p>Everyday (10.00-22.00)</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6">
                    <div class="about_info">
                        <div class="section_title mb-20px">
                            <span>About Us</span>
                        </div>
                        <p>Outlookbarbershop berdiri tahun 2018 di Sukodono. Outlook adalah perusahaan yang berjalan di bidang pelayanan atau jasa potong rambut profesional yang berkualitas dengan fasilitas yang memadai. Diera Digital ini Outlook barbershop telah mengembangkan digital member untuk pelanggan setia.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- about_area_end -->
<div class="service_area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section_title2 text-center mb-90">
                    <i class="flaticon-scissors"></i>
                    <h3>our service</h3>
                </div>
            </div>
        </div>
        <div class="white_bg_pos">
            <div class="row">
                <div class="col-xl-6">
                    <div class="single_service d-flex justify-content-between align-items-center">
                        <div class="service_inner d-flex align-items-center">
                            <div class="thumb">
                                    <img src="{{ asset('assets/img/service/1.png') }}" alt="">
                            </div>
                            <span>Hair Cut</span>
                        </div>
                        <p>………………..35k - 50k</p>
                    </div>
                    <div class="single_service d-flex justify-content-between align-items-center">
                        <div class="service_inner d-flex align-items-center">
                            <div class="thumb">
                                    <img src="{{ asset('assets/img/service/2.png') }}" alt="">
                            </div>
                            <span>Shaving</span>
                        </div>
                        <p>………………....5k - 10k</p>
                    </div>
                    <div class="single_service d-flex justify-content-between align-items-center">
                        <div class="service_inner d-flex align-items-center">
                            <div class="thumb">
                                    <img src="{{ asset('assets/img/service/3.png') }}" alt="">
                            </div>
                            <span>Massage/Creambath</span>
                        </div>
                        <p>………………..10k - 50k</p>
                    </div>
                    <div class="single_service d-flex justify-content-between align-items-center">
                        <div class="service_inner d-flex align-items-center">
                            <div class="thumb">
                                    <img src="{{ asset('assets/img/service/4.png') }}" alt="">
                            </div>
                            <span>Masker Wajah</span>
                        </div>
                        <p>……………………10k - 50k</p>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="single_service d-flex justify-content-between align-items-center">
                        <div class="service_inner d-flex align-items-center">
                            <div class="thumb">
                                    <img src="{{ asset('assets/img/service/5.png') }}" alt="">
                            </div>
                            <span>Bleaching</span>
                        </div>
                        <p>………………….50k – 100k</p>
                    </div>
                    <div class="single_service d-flex justify-content-between align-items-center">
                        <div class="service_inner d-flex align-items-center">
                            <div class="thumb">
                                    <img src="{{ asset('assets/img/service/7.png') }}" alt="">
                            </div>
                            <span>Smoothing</span>
                        </div>
                        <p>……………………..70k - 150k</p>
                    </div>
                    <div class="single_service d-flex justify-content-between align-items-center">
                        <div class="service_inner d-flex align-items-center">
                            <div class="thumb">
                                    <img src="{{ asset('assets/img/service/6.png') }}" alt="">
                            </div>
                            <span>Toning</span>
                        </div>
                        <p>………………………………50k</p>
                    </div>
                    <div class="single_service d-flex justify-content-between align-items-center">
                        <div class="service_inner d-flex align-items-center">
                            <div class="thumb">
                                    <img src="{{ asset('assets/img/service/7.png') }}" alt="">
                            </div>
                            <span>Colouring</span>
                        </div>
                        <p>………………150k - 250k</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- gallery_area_start -->
    <div class="gallery_area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section_title2 text-center mb-90">
                        <i class="flaticon-scissors"></i>
                        <h3 style="color: black;">Our Gallery</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="gallery_active owl-carousel">
                        <div class="single_gallery">
                            <div class="thumb">
                                <img src="{{ asset('assets/img/gallery/1.jpg') }}" alt="">
                                <div class="image_hover">
                                    <a class="popup-image" href="{{ asset('assets/img/gallery/1.jpg') }}">
                                        <i class="ti-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="single_gallery">
                            <div class="thumb">
                                <img src="{{ asset('assets/img/gallery/2.jpg') }}" alt="">
                                <div class="image_hover">
                                    <a class="popup-image" href="{{ asset('assets/img/gallery/2.jpg') }}">
                                        <i class="ti-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="single_gallery">
                            <div class="thumb">
                                <img src="{{ asset('assets/img/gallery/3.jpg') }}" alt="">
                                <div class="image_hover">
                                    <a class="popup-image" href="{{ asset('assets/img/gallery/3.jpg') }}">
                                        <i class="ti-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="single_gallery">
                            <div class="thumb">
                                <img src="{{ asset('assets/img/gallery/1.jpg') }}" alt="">
                                <div class="image_hover">
                                    <a class="popup-image" href="{{ asset('assets/img/gallery/1.jpg') }}">
                                        <i class="ti-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="single_gallery">
                            <div class="thumb">
                                <img src="{{ asset('assets/img/gallery/2.jpg') }}" alt="">
                                <div class="image_hover">
                                    <a class="popup-image" href="{{ asset('assets/img/gallery/2.jpg') }}">
                                        <i class="ti-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="single_gallery">
                            <div class="thumb">
                                <img src="{{ asset('assets/img/gallery/3.jpg') }}" alt="">
                                <div class="image_hover">
                                    <a class="popup-image" href="{{ asset('assets/img/gallery/3.jpg') }}">
                                        <i class="ti-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- gallery_area_end -->

    <!-- video_area_start -->
    <div class="video_area">
        <div class="container-fluid p-0">
            <div class="row align-items-center no-gutters">
                <div class="col-xl-6 col-lg-6">
                    <div class="video_info">
                        <div class="about_info">
                            <div class="section_title mb-20px">
                                <span>How we Work</span>
                                <h3>Watch the Video <br>
                                    How we Work?</h3>
                            </div>
                            <p>Inspires employees and organizations to support causes they care <br>
                                about. We do this to bring more resources to the nonprofits that are <br>
                                changing our world.</p>
                            <a href="#" class="boxed-btn3">book now</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6">
                    <div class="video_thumb">
                        <div class="video_thumb_inner">
                            <img src="{{ asset('assets/img/gallery/video.png') }}" alt="">
                            <a href="https://www.youtube.com/watch?v=I4NcwG-zusE" class="popup-video">
                                <i class="fa fa-play"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- video_area_end -->

    <!-- cutter_muster_start -->
    <!-- <div class="cutter_muster">
        <div class="container">
                <div class="row">
                        <div class="col-xl-12">
                            <div class="section_title2 text-center mb-90">
                                <i class="flaticon-scissors"></i>
                                <h3>Our Cutter Masters</h3>
                            </div>
                        </div>
                    </div>
            <div class="row">
                <div class="col-xl-3 col-md-6 col-lg-3">
                    <div class="single_master">
                        <div class="thumb">
                            <img src="img/team/1.png" alt="">
                            <div class="social_link">
                                <a href="#"><i class="fa fa-envelope"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                        <div class="master_name text-center">
                            <h3>Macau Wilium</h3>
                            <p>Massage Master</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-lg-3">
                    <div class="single_master">
                        <div class="thumb">
                            <img src="img/team/2.png" alt="">
                            <div class="social_link">
                                <a href="#"><i class="fa fa-envelope"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                        <div class="master_name text-center">
                            <h3>Dan Jacky</h3>
                            <p>Mens Cut</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-lg-3">
                    <div class="single_master">
                        <div class="thumb">
                            <img src="img/team/3.png" alt="">
                            <div class="social_link">
                                <a href="#"><i class="fa fa-envelope"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                        <div class="master_name text-center">
                            <h3>Luka Luka</h3>
                            <p>Mens Cut</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-lg-3">
                    <div class="single_master">
                        <div class="thumb">
                            <img src="img/team/4.png" alt="">
                            <div class="social_link">
                                <a href="#"><i class="fa fa-envelope"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                        <div class="master_name text-center">
                            <h3>Rona Dana</h3>
                            <p>Ladies Cut</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- cutter_muster_end -->

    <!-- find_us_area start -->
    <div class="find_us_area find_bg_1 ">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 offset-xl-7 col-lg-6 offset-lg-6">
                    <div class="find_info">
                        <h3 class="find_info_title">How to Find Us</h3>
                        <div class="single_find d-flex">
                            <div class="icon">
                                <i class="flaticon-placeholder"></i>
                            </div>
                            <div class="find_text">
                                    <h3>Location</h3>
                                    <p style="font-size: 15px;">Outlook 1.0 : Jl. Karangnongko lt.2 (sebelah Indomaret) Sukodono, Sidoarjo.</p>
                                    <p style="font-size: 15px;">Outlook 2.0 : Jl. Taman Pondok Jati No. 7, Geluran, Taman, Sidoarjo.</p>
                            </div>
                        </div>
                        <div class="single_find d-flex">
                            <div class="icon">
                                <i class="flaticon-phone-call"></i>
                            </div>
                            <div class="find_text">
                                    <h3>Call Us</h3>
                                    <p style="font-size: 15px;">+62 852 583 020 01</p>
                            </div>
                        </div>
                        <div class="single_find d-flex">
                            <div class="icon">
                                <i class="flaticon-paper-plane"></i>
                            </div>
                            <div class="find_text">
                                    <h3>Mail Us</h3>
                                    <p style="font-size: 15px;">Outlookbarbershop@gmail.com</p>
                            </div>
                        </div>
                        <div class="single_find d-flex">
                            <div class="icon">
                                        <i class="fa fa-instagram"></i>
                            </div>
                            <div class="find_text">
                                    <h3>Instagram</h3>
                                    <p style="font-size: 15px;">@outlookbarbershop</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- find_us_area_end -->

    <!-- footer -->
    <footer class="footer">
            <div class="footer_top">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-2 col-md-6 col-lg-2">
                            <div class="footer_widget">
                                <h3 class="footer_title">
                                    Join With Us
                                </h3>
                                <p class="footer_text doanar">
                                    <a href="https://api.whatsapp.com/send/?phone=62852583020 01&text&app_absent=0">+62 852 583 020 01</a></p>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-lg-2"></div>
                        <div class="col-xl-4 col-md-6 col-lg-4">
                            <div class="footer_widget">
                                <h3 class="footer_title">
                                    address
                                </h3>
                                <p class="footer_text">Outlook 1.0 : Jl. Karangnongko lt.2 (sebelah Indomaret) Sukodono, Sidoarjo.
                                    <br>
                                    Outlook 2.0 : Jl. Taman Pondok Jati No. 7, Geluran, Taman, Sidoarjo.<br>
                            </div>
                        </div>
                         <div class="col-xl-2 col-md-6 col-lg-2"></div>
                        <div class="col-xl-2 col-md-6 col-lg-2">
                            <div class="footer_widget">
                                <h3 class="footer_title">
                                    Social Media
                                </h3>
                                </p>

                                <ul>
                                    <li>
                                        <a style="color: #B2B2B2;" href = "mailto: Outlookbarbershop@gmail.com"> Email:
                                            <b>Outlookbarbershop@gmail.com</b>
                                        </a>
                                    </li>
                                    <li>
                                        <a style="color: #B2B2B2;" href="https://www.instagram.com/outlookbarbershop/">Instagram:
                                            <b>@outlookbarbershop</b>
                                        </a>
                                    </li>
                                    <li>
                                        <a style="color: #B2B2B2;" href="https://www.tiktok.com/@outlookbarbershop?_t=8adVUHHZ8vy&_r=1">Tiktok:
                                            <b>outlookbarbershop</b>
                                        </a>
                                    </li>
                                    <li>
                                        <a style="color: #B2B2B2;" href="https://youtube.com/@OutlookBarbershop">Youtube:
                                            <b>OutlookBarbershop</b>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copy-right_text">
                <div class="container">
                    <div class="footer_border"></div>
                    <div class="row">
                        <div class="col-xl-12">
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    <!-- footer -->
    <!-- link that opens popup -->

    <!-- JS here -->
    <script src="{{ asset('assets/js/vendor/modernizr-3.5.0.min.js') }}"></script>
    <script src="{{ asset('assets/js/vendor/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/js/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/js/ajax-form.js') }}"></script>
    <script src="{{ asset('assets/js/waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('assets/js/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/js/scrollIt.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.scrollUp.min.js') }}"></script>
    <script src="{{ asset('assets/js/wow.min.js') }}"></script>
    <script src="{{ asset('assets/js/nice-select.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.slicknav.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins.js') }}"></script>
    <script src="{{ asset('assets/js/gijgo.min.js') }}"></script>

    <!--contact js-->
    <script src="{{ asset('assets/js/contact.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.form.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/mail-script.js') }}"></script>

    
    <script>
        $('#datepicker').datepicker({
            iconsLibrary: 'fontawesome',
            disableDaysOfWeek: [0, 0],
        //     icons: {
        //      rightIcon: '<span class="fa fa-caret-down"></span>'
        //  }
        });
        $('#datepicker2').datepicker({
            iconsLibrary: 'fontawesome',
            icons: {
             rightIcon: '<span class="fa fa-caret-down"></span>'
         }

        });
        var timepicker = $('#timepicker').timepicker({
         format: 'HH.MM'
     });
    </script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
</body>

</html>