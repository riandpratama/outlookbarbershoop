<!-- footer -->
<footer class="footer">
    <div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-md-6 col-lg-3">
                    <div class="footer_widget">
                        <h3 class="footer_title">
                            Kontak Kami
                        </h3>
                        <p class="footer_text doanar"> <a class="first" href="#">Make Appointment</a> <br>
                        <a href="#">085-258-302-001</a></p>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-lg-3">
                    <div class="footer_widget">
                        <h3 class="footer_title">Alamat</h3>
                        <p class="footer_text">JL KARANGNONGKO (LT2) NO. 7 SUKODONO SIDOARJO <br></p>
                        <p class="footer_text">JL TAMAN PONDOK JATI BLOK C NO.7 GELURAN SIDOARJO <br></p>
                        <div class="socail_links">
                            <ul>
                                <li>
                                    <a href="https://www.instagram.com/outlookbarbershop/">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <div class="footer_widget">
                        <h3 class="footer_title">
                        Newsletter
                        </h3>
                        <form action="#" class="newsletter_form">
                            <input type="text" placeholder="Enter your mail">
                            <button type="submit">Sign Up</button>
                        </form>
                        <p class="newsletter_text">Subscribe newsletter to get updates</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="copy-right_text">
        <div class="container">
            <div class="footer_border"></div>
            <div class="row">
                <div class="col-xl-12">
                    <p class="copy_right text-center">

                    Copyright ©
                    <script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script>
                    <script type="text/javascript">document.write(new Date().getFullYear());</script> 
                    All rights reserved | OutlookBarbershop</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer -->