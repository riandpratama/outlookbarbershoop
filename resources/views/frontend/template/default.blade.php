<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	@include('frontend.template.partials._head')
	<body>
		
		@include('frontend.template.partials._header')
		
			
		@yield('content')
		
		@include('frontend.template.partials._footer')
			
		@include('frontend.template.partials._javascript')
	</body>
	
</html>