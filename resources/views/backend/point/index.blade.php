@extends('adminlte::page')

@section('title', 'Setting')

@section('content_header')
<h1>Setting Point (rupiah) & Batas Penukaran Rupiah</h1>
@stop

@section('content')
<div class="row" style="padding-top: 20px;">
  <div class="col-md-12">
    <hr>
    <div class="row">
      <div class="col-sm-6 table-responsive">
        <h3>
          <b>Setting Point (Rupiah)</b>
          <button data-title="Add" data-toggle="modal" data-target="#add" class="btn btn-xs btn-primary">
            <i class="fa fa-plus"></i> Tambah
          </button>
        </h3>
        <hr>
        <table id="barberman-datatables" class="table table-responsive table-striped table-bordered table-hover"
          width="100%" style="text-align: center;">
          <thead>
            <tr>
              <th>No</th>
              <th>
                <center>Point (Rupiah)</center>
              </th>
              <th>
                <center>Aksi</center>
              </th>
            </tr>
          </thead>
          <tbody>
            @foreach ($pointSelect as $item)
            <tr>
              <td>
                {{$loop->iteration}}.
              </td>
              <td>
                <center>{{number_format($item->point, 2)}}</center>
              </td>
              <td>
                <center>
                  <button data-title="Edit" data-toggle="modal" data-target="#edit" data-item="{{json_encode($item)}}"
                    onclick="edit(this)" class="btn btn-xs btn-info"><i class="fa fa-edit"></i>
                  </button>
                  <form action="{{route('point.destroy', $item->id)}}" method="get">
                  <button class="btn btn-xs btn-danger" type="submit"><i class="fa fa-trash"></i>
                  </button>
                  </form>
                </center>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>

      <div class="col-sm-6 table-responsive">
        <h3><b>Batas Penukaran Rupiah</b></h3>
        <hr>
        <table id="point-datatables" class="table table-responsive table-striped table-bordered table-hover"
          width="100%" style="text-align: center;">
          <thead>
            <tr>
              <th>#</th>
              <th>
                <center>Point (Rupiah)</center>
              </th>
              <th>
                <center>Aksi</center>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>-</td>
              <td>
                <center>{{number_format($point->point, 2)}}</center>
              </td>
              <td>
                <center>
                  <button data-title="Edit" data-toggle="modal" data-target="#edit" data-item="{{json_encode($point)}}"
                    onclick="edit(this)" class="btn btn-xs btn-info"><i class="fa fa-edit"></i>
                  </button>
                </center>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4><i class="fa fa-plus"></i> Data Point</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
          <form id="formEdit" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('point') ? ' has-error' : '' }}">
              <label for="point">Point</label>
              <input id="point" type="text" class="form-control" name="point" value="{{ old('point') }}">
              @if ($errors->has('point'))
              <span class="help-block">
                <strong>{{ $errors->first('point') }}</strong>
              </span>
              @endif
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-primary"
                onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span
                  class="glyphicon glyphicon-save"></span> Simpan</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"
                  aria-hidden="true"></span> Batal</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Add -->
  <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="add" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4><i class="fa fa-plus"></i> Data Point</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
          <form id="formAdd" method="POST" enctype="multipart/form-data" action="{{ route('point.store') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('point') ? ' has-error' : '' }}">
              <label for="point">Point</label>
              <input id="point" type="text" class="form-control" name="point" value="{{ old('point') }}">
              @if ($errors->has('point'))
              <span class="help-block">
                <strong>{{ $errors->first('point') }}</strong>
              </span>
              @endif
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-primary"
                onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span
                  class="glyphicon glyphicon-save"></span> Simpan</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"
                  aria-hidden="true"></span> Batal</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

</div>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
<script type="text/javascript">
  $(function () {
        $('#barberman-datatables').DataTable();
    });

    $(function () {
        $('#point-datatables').DataTable();
    });
</script>
<script type="text/javascript">
  edit = (button) => {
      var item = $(button).data('item');

      $('form#formEdit').attr('action','{{ url("backend/point") }}/'+item.id+'/update');
      $('#formEdit .form-group #id').val(item.id);
      $('#formEdit .form-group #point').val(item.point);
  }
</script>

@endsection