@extends('adminlte::page')

@section('title', 'Barberman')

@section('content_header')
<h1>Barberman</h1>
@stop

@section('content')
<div class="row" style="padding-top: 20px;">
  <div class="col-md-12">
    <button data-title="Tambah" data-toggle="modal" data-target="#tambah" onclick="tambah(this)"
      class="btn btn-xs btn-info"><i class="fa fa-plus"> Tambah Data Barberman</i>
    </button>
    <hr>
    <div class="row">

      <div class="col-sm-12 table-responsive">
        <table id="barberman-datatables" class="table table-responsive table-striped table-bordered table-hover"
          width="100%" style="text-align: center;">
          <thead>
            <tr>
              <th>
                <center>No.</center>
              </th>
              <th>
                <center>Photo</center>
              </th>
              <th>
                <center>Barberman</center>
              </th>
              <th>
                <center>Email</center>
              </th>
              <th>
                <center>Lokasi</center>
              </th>
              <th>
                <center>Discount</center>
              </th>
              <th>
                <center>Price</center>
              </th>
              <th>
                <center>Price Discount</center>
              </th>
              <th>
                <center>Service</center>
              </th>
              <th>
                <center>Available Gender</center>
              </th>
              <th>
                <center>Status</center>
              </th>
              <th>
                <center>Aksi</center>
              </th>
            </tr>
          </thead>
          <tbody>
            @foreach ($barbermans as $item)
            <tr>
              <td>
                <center>{{$loop->iteration}}</center>
              </td>
              <td>
                <img src="{{ asset('storage/'.$item->photo)}}" width="50" />
              </td>
              <td>
                <center>{{$item->name}}</center>
              </td>
              <td>
                <center>{{$item->email}}</center>
              </td>
              <td>
                <center>{{$item->location}}</center>
              </td>
              <td>
                <center>
                  @if($item->service == 1)
                    {{$item->discount}} %
                  @else
                    *Harga mengikuti service
                  @endif
                </center>
              </td>
              <td>
                <center>
                  @if($item->service == 1)
                    {{number_format($item->price, 2)}}
                  @else
                    *Harga mengikuti service
                  @endif
                </center>
              </td>
              <td>
                <center>
                  @if($item->service == 1)
                    {{number_format($item->price_discount, 2)}}
                  @else
                    *Harga mengikuti service
                  @endif
                </center>
              </td>
              <td>
                <center>
                  @if($item->service == 1) Haircut @else Treatment @endif
                </center>
              </td>
              <td>
                <center>
                  {{$item->available_gender}}
                </center>
              </td>
              <td>
                <center>
                  @if($item->status == 1) Aktif @else Tidak Aktif @endif
                </center>
              </td>
              <td>
                <center>
                  <button data-title="Edit" data-toggle="modal" data-target="#edit" data-item="{{json_encode($item)}}"
                    onclick="edit(this)" class="btn btn-xs btn-info"><i class="fa fa-edit"></i>
                  </button>
                </center>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>

    </div>
  </div>

  <!-- Modal Edit -->
  <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4><i class="fa fa-plus"></i> Data Barberman</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
          <form id="formEdit" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              <label for="name">Nama Barberman</label>
              <input id="id" type="hidden" name="id">
              <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
              @if ($errors->has('name'))
              <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email">Email Barberman</label>
              <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}">
              @if ($errors->has('email'))
              <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('discount') ? ' has-error' : '' }}">
              <label for="discount">Discount</label>
              <input id="discount" type="number" class="form-control" name="discount" value="{{ old('discount') }}" max="100">
              @if ($errors->has('discount'))
              <span class="help-block">
                <strong>{{ $errors->first('discount') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
              <label for="price">Price (Harga)</label>
              <input id="price" type="text" class="form-control" name="price" value="{{ old('price') }}">
              @if ($errors->has('price'))
              <span class="help-block">
                <strong>{{ $errors->first('price') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
              <label for="status">Status</label>
              <select id="status" class="form-control" name="status">
                <option value="1">Aktif</option>
                <option value="0">Tidak Aktif</option>
              </select>
              @if ($errors->has('status'))
              <span class="help-block">
                <strong>{{ $errors->first('status') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('location_id') ? ' has-error' : '' }}">
              <label for="location_id">Lokasi Barbershop</label>
              <select id="location_id" class="form-control" name="location_id">
                @foreach ($locations as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
              </select>
              @if ($errors->has('location_id'))
              <span class="help-block">
                <strong>{{ $errors->first('location_id') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('service') ? ' has-error' : '' }}">
              <label for="service">Service</label>
              <select id="service" class="form-control" name="service">
                <option value="1">Haircut</option>
                <option value="2">Treatment</option>
              </select>
              @if ($errors->has('service'))
              <span class="help-block">
                <strong>{{ $errors->first('service') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('available_gender') ? ' has-error' : '' }}">
              <label for="available_gender">Available Gender</label>
              <select id="available_gender" class="form-control" name="available_gender">
                <option value="all">all</option>
                <option value="man">man</option>
                <option value="woman">woman</option>
              </select>
              @if ($errors->has('available_gender'))
              <span class="help-block">
                <strong>{{ $errors->first('available_gender') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
              <label for="photo">Photo</label>
              <input id="photo" type="file" class="form-control" name="photo" value="{{ old('photo') }}">
              @if ($errors->has('photo'))
              <span class="help-block">
                <strong>{{ $errors->first('photo') }}</strong>
              </span>
              @endif
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-primary"
                onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span
                  class="glyphicon glyphicon-save"></span> Simpan</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"
                  aria-hidden="true"></span> Batal</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Tambah-->
  <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambah" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4><i class="fa fa-plus"></i> Tambah Barberman</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
          <form id="formTambah" method="POST" enctype="multipart/form-data" action="{{route('barberman.store')}}">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              <label for="name">Nama Barberman</label>
              <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
              @if ($errors->has('name'))
              <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email">Email Barberman</label>
              <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}">
              @if ($errors->has('email'))
              <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
              <label for="price">Price (Harga)</label>
              <input id="price" type="text" class="form-control" name="price" value="{{ old('price') }}">
              <i style="color:red;">*Input harga hanya untuk type Haircut (Treatment harga di input di service)</i>
              @if ($errors->has('price'))
              <span class="help-block">
                <strong>{{ $errors->first('price') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('location_id') ? ' has-error' : '' }}">
              <label for="location_id">Lokasi Barbershop</label>
              <select id="location_id" class="form-control" name="location_id">
                @foreach ($locations as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
              </select>
              @if ($errors->has('location_id'))
              <span class="help-block">
                <strong>{{ $errors->first('location_id') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('service') ? ' has-error' : '' }}">
              <label for="service">Service</label>
              <select id="service" class="form-control" name="service">
                <option value="1">Haircut</option>
                <option value="2">Treatment</option>
              </select>
              @if ($errors->has('service'))
              <span class="help-block">
                <strong>{{ $errors->first('service') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('available_gender') ? ' has-error' : '' }}">
              <label for="available_gender">Available Gender</label>
              <select id="available_gender" class="form-control" name="available_gender">
                <option value="all">All</option>
                <option value="man">Man</option>
                <option value="woman">Woman</option>
              </select>
              @if ($errors->has('available_gender'))
              <span class="help-block">
                <strong>{{ $errors->first('available_gender') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
              <label for="photo">Photo</label>
              <input id="photo" type="file" class="form-control" name="photo" value="{{ old('photo') }}">
              @if ($errors->has('photo'))
              <span class="help-block">
                <strong>{{ $errors->first('photo') }}</strong>
              </span>
              @endif
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-primary" onclick="return confirm('Anda yakin ingin menyelesaikan?')">
                <span class="glyphicon glyphicon-save"></span> Simpan
              </button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">
                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
  <script type="text/javascript">
    $(function () {
        $('#barberman-datatables').DataTable();
    });
  </script>

  <script type="text/javascript">
    edit = (button) => {
        var item = $(button).data('item');

        $('form#formEdit').attr('action','{{ url("backend/barbermans") }}/'+item.id+'/update');
        $('#formEdit .form-group #id').val(item.id);
        $('#formEdit .form-group #name').val(item.name);
        $('#formEdit .form-group #email').val(item.email);
        $('#formEdit .form-group #discount').val(item.discount);
        $('#formEdit .form-group #price').val(item.price);
        $('#formEdit .form-group #status').val(item.status);
        $('#formEdit .form-group #service').val(item.service);
        $('#formEdit .form-group #location_id').val(item.location_id);
        $('#formEdit .form-group #available_gender').val(item.available_gender);
    }
  </script>

  @endsection