@extends('adminlte::page')

@section('title', 'Description')

@section('content_header')
  <h1>Description</h1>
@stop

@section('content')
<div class="row" style="padding-top: 20px;">
  <div class="col-md-12">
    <hr>
    <div class="row">

      <div class="col-sm-12 table-responsive">
        <table id="barberman-datatables" class="table table-responsive table-striped table-bordered table-hover"
          width="100%" style="text-align: center;">
          <thead>
            <tr>
              <th>
                <center>#</center>
              </th>
              <th>
                <center>Top Description</center>
              </th>
              <th>
                <center>Middle Description</center>
              </th>
              <th>
                <center>Bottom Description</center>
              </th>
              <th>
                <center>Aksi</center>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <center>-</center>
              </td>
              <td>
                <center>{{$description->top_desc}}</center>
              </td>
              <td>
                <center>{{$description->middle_desc}}</center>
              </td>
              <td>
                <center>{{$description->bottom_desc}}</center>
              </td>
              <td>
                <center>
                  <button data-title="Edit" data-toggle="modal" data-target="#edit" data-item="{{json_encode($description)}}"
                    onclick="edit(this)" class="btn btn-xs btn-info"><i class="fa fa-edit"></i>
                  </button>
                </center>
              </td>
            </tr>
          </tbody>
        </table>
      </div>

    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4><i class="fa fa-plus"></i> Data Description</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
          <form id="formEdit" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('top_desc') ? ' has-error' : '' }}">
              <label for="top_desc">Top Description</label>
              <input id="top_desc" type="text" class="form-control" name="top_desc" value="{{ old('top_desc') }}">
              @if ($errors->has('top_desc'))
              <span class="help-block">
                <strong>{{ $errors->first('top_desc') }}</strong>
              </span>
              @endif
            </div>

            <div class="form-group{{ $errors->has('middle_desc') ? ' has-error' : '' }}">
              <label for="middle_desc">Top Description</label>
              <input id="middle_desc" type="text" class="form-control" name="middle_desc" value="{{ old('middle_desc') }}">
              @if ($errors->has('middle_desc'))
              <span class="help-block">
                <strong>{{ $errors->first('middle_desc') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('bottom_desc') ? ' has-error' : '' }}">
              <label for="bottom_desc">Top Description</label>
              <input id="bottom_desc" type="text" class="form-control" name="bottom_desc" value="{{ old('bottom_desc') }}">
              @if ($errors->has('bottom_desc'))
              <span class="help-block">
                <strong>{{ $errors->first('bottom_desc') }}</strong>
              </span>
              @endif
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-primary"
                onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span
                  class="glyphicon glyphicon-save"></span> Simpan</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"
                  aria-hidden="true"></span> Batal</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

</div>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
<script type="text/javascript">
  $(function () {
        $('#barberman-datatables').DataTable();
    });
</script>
<script type="text/javascript">
  edit = (button) => {
      var item = $(button).data('item');

      $('form#formEdit').attr('action','{{ url("backend/description") }}/'+item.id+'/update');
      $('#formEdit .form-group #id').val(item.id);
      $('#formEdit .form-group #top_desc').val(item.top_desc);
      $('#formEdit .form-group #middle_desc').val(item.middle_desc);
      $('#formEdit .form-group #bottom_desc').val(item.bottom_desc);
  }
</script>

@endsection