@extends('adminlte::page')

@section('title', 'Time')

@section('content_header')
<h1>Time (Waktu)</h1>
@stop

@section('content')
<div class="row" style="padding-top: 20px;">
  <div class="col-md-12">
    <button data-title="Tambah" data-toggle="modal" data-target="#tambah" onclick="tambah(this)"
      class="btn btn-xs btn-info"><i class="fa fa-plus"> Tambah Data</i>
    </button>
    <hr>
    <div class="row">

      <div class="col-sm-12 table-responsive">
        <table id="barberman-datatables" class="table table-responsive table-striped table-bordered table-hover"
          width="100%" style="text-align: center;">
          <thead>
            <tr>
              <th>
                <center>No.</center>
              </th>
              <th>
                <center>Time</center>
              </th>
              <th>
                <center>Aksi</center>
              </th>
            </tr>
          </thead>
          <tbody>
            @foreach ($times as $item)
            <tr>
              <td>
                <center>{{$loop->iteration}}</center>
              </td>
              <td>
                <center>{{$item->time}}</center>
              </td>
              <td>
                <center>
                  <button data-title="Edit" data-toggle="modal" data-target="#edit" data-item="{{json_encode($item)}}"
                    onclick="edit(this)" class="btn btn-xs btn-info"><i class="fa fa-edit"></i>
                  </button>
                </center>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>

    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4><i class="fa fa-plus"></i> Data Time</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
          <form id="formEdit" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('time') ? ' has-error' : '' }}">
              <label for="time">Time</label>
              <input id="id" type="hidden" name="id">
              <input id="time" type="text" class="form-control" name="time" value="{{ old('time') }}">
              @if ($errors->has('time'))
              <span class="help-block">
                <strong>{{ $errors->first('time') }}</strong>
              </span>
              @endif
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-primary"
                onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span
                  class="glyphicon glyphicon-save"></span> Simpan</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"
                  aria-hidden="true"></span> Batal</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Tambah-->
  <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambah" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4><i class="fa fa-plus"></i> Tambah Barberman</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
          <form id="formTambah" method="POST" enctype="multipart/form-data" action="{{route('times.store')}}">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('time') ? ' has-error' : '' }}">
              <label for="time">Time (Waktu)</label>
              <input id="time" type="text" class="form-control" name="time" value="00:00:00">
              @if ($errors->has('time'))
              <span class="help-block">
                <strong>{{ $errors->first('time') }}</strong>
              </span>
              @endif
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-primary" onclick="return confirm('Anda yakin ingin menyelesaikan?')">
                <span class="glyphicon glyphicon-save"></span> Simpan
              </button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">
                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

</div>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
<script type="text/javascript">
  $(function () {
        $('#barberman-datatables').DataTable({
          "lengthMenu": [[50, 100, -1], [50, 100, "All"]]
        });
    });
</script>

<script type="text/javascript">
  edit = (button) => {
      var item = $(button).data('item');

      $('form#formEdit').attr('action','{{ url("backend/times") }}/'+item.id+'/update');
      $('#formEdit .form-group #id').val(item.id);
      $('#formEdit .form-group #time').val(item.time);
  }
</script>

@endsection