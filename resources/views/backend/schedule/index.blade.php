@extends('adminlte::page')

@section('title', 'Schedule (Jadwal)')

@section('content_header')
<h1>Schedule (Jadwal)</h1>
@stop

@section('content')
<div class="row" style="padding-top: 20px;">
  <div class="col-md-12">
    <button data-title="Tambah" data-toggle="modal" data-target="#tambah" onclick="tambah(this)"
      class="btn btn-xs btn-info"><i class="fa fa-plus"> Tambah Data Jadwal</i>
    </button>
    <hr>
    <div class="row">

      <ul class="nav nav-tabs bordered">
        <li class="active"><a href="#link_1" data-toggle="tab"> Pekarungan, Kec. Sukodono</a></li>
        <li><a href="#link_2" data-toggle="tab">Geluran, Kec. Taman</a></li>
      </ul>
      <br><br>

      <div class="tab-content padding-10">
        <div class="tab-pane active" id="link_1">
          <div class="col-sm-12 table-responsive">
            <table id="barberman-datatables" class="table table-responsive table-striped table-bordered table-hover"
              width="100%" style="text-align: center;">
              <thead>
                <tr>
                  <th>
                    <center>No.</center>
                  </th>
                  <th>
                    <center>Barberman</center>
                  </th>
                  <th>
                    <center>Service</center>
                  </th>
                  <th>
                    <center>Status</center>
                  </th>
                  <th>
                    <center>Aksi</center>
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach ($barbermans_lokasi_1 as $item)
                <tr>
                  <td>
                    <center>{{$loop->iteration}}</center>
                  </td>
                  <td>
                    <center>{{$item->name}}</center>
                  </td>
                  <td>
                    <center>
                      @if($item->service == 1) Haircut @else Treatment @endif
                    </center>
                  </td>
                  <td>
                    <center>
                      @if($item->status == 1) Aktif @else Tidak Aktif @endif
                    </center>
                  </td>
                  <td>
                    <center>
                      <a href="{{route('schedule.show', $item->id)}}" class="btn btn-xs btn-success"><i
                          class="fa fa-eye"></i>
                      </a>
                    </center>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="tab-pane" id="link_2">
          <div class="col-sm-12 table-responsive">
            <table id="barberman2-datatables" class="table table-responsive table-striped table-bordered table-hover"
              width="100%" style="text-align: center;">
              <thead>
                <tr>
                  <th>
                    <center>No.</center>
                  </th>
                  <th>
                    <center>Barberman</center>
                  </th>
                  <th>
                    <center>Service</center>
                  </th>
                  <th>
                    <center>Status</center>
                  </th>
                  <th>
                    <center>Aksi</center>
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach ($barbermans_lokasi_2 as $item)
                <tr>
                  <td>
                    <center>{{$loop->iteration}}</center>
                  </td>
                  <td>
                    <center>{{$item->name}}</center>
                  </td>
                  <td>
                    <center>
                      @if($item->service == 1) Haircut @else Treatment @endif
                    </center>
                  </td>
                  <td>
                    <center>
                      @if($item->status == 1) Aktif @else Tidak Aktif @endif
                    </center>
                  </td>
                  <td>
                    <center>
                      <a href="{{route('schedule.show', $item->id)}}" class="btn btn-xs btn-success"><i
                          class="fa fa-eye"></i>
                      </a>
                    </center>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambah" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4><i class="fa fa-plus"></i> Data Barberman</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
          <form id="formTambah" method="POST" enctype="multipart/form-data" action="{{route('schedule.store')}}">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              <label for="name">Nama Barberman</label>
              <select class="form-control" name="barberman_id">
                @foreach($barbermans_all as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
              </select>
              @if ($errors->has('name'))
              <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
              @endif
            </div>

            <div class="form-group{{ $errors->has('day_id') ? ' has-error' : '' }}">
              <label for="day_id">Pilih Hari</label>
              <select class="form-control" name="day_id">
                <option value="1">Senin</option>
                <option value="2">Selasa</option>
                <option value="3">Rabu</option>
                <option value="4">Kamis</option>
                <option value="5">Jumat</option>
                <option value="6">Sabtu</option>
                <option value="7">Minggu</option>
              </select>
              @if ($errors->has('day_id'))
              <span class="help-block">
                <strong>{{ $errors->first('day_id') }}</strong>
              </span>
              @endif
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-primary"
                onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span
                  class="glyphicon glyphicon-save"></span> Simpan</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"
                  aria-hidden="true"></span> Batal</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
  <script type="text/javascript">
    $(function () {
      $('#barberman-datatables').DataTable();
      $('#barberman2-datatables').DataTable();
  });
  </script>

  @endsection