@extends('adminlte::page')

@section('title', 'Schedule (Jadwal)')

@section('content_header')
<a href="{{route('schedule.index')}}" class="btn btn-primary btn-sm"><i class="fa fa-arrow-left"></i> <b>Kembali</b></a>
<h3>Detail Schedule (Jadwal Detil) <br> <b>- {{$data->name}} -</b></h3>
@stop

<style>
  .switch {
    position: relative;
    display: inline-block;
    width: 60px;
    height: 34px;
  }

  .switch input {
    opacity: 0;
    width: 0;
    height: 0;
  }

  .slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #ccc;
    -webkit-transition: .4s;
    transition: .4s;
  }

  .slider:before {
    position: absolute;
    content: "";
    height: 26px;
    width: 26px;
    left: 4px;
    bottom: 4px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;
  }

  input:checked+.slider {
    background-color: #050607;
  }

  input:focus+.slider {
    box-shadow: 0 0 1px #050607;
  }

  input:checked+.slider:before {
    -webkit-transform: translateX(26px);
    -ms-transform: translateX(26px);
    transform: translateX(26px);
  }

  /* Rounded sliders */
  .slider.round {
    border-radius: 34px;
  }

  .slider.round:before {
    border-radius: 50%;
  }
</style>

@section('content')

<div class="row" style="padding-top: 20px;">
  <div class="col-md-12">
    <div class="row">

      @include('backend.schedule.day.monday')

      @include('backend.schedule.day.tuesday')

      @include('backend.schedule.day.wednesday')

      <hr>

      @include('backend.schedule.day.thursday')

      @include('backend.schedule.day.friday')

      @include('backend.schedule.day.saturday')

      <hr>

      @include('backend.schedule.day.sunday')

    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <script type="text/javascript">
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    function update(id) {
      var url = '{{ route("schedule.update", ":id") }}';
      url = url.replace(':id', id );
      var val = 0;

      if ($(`#status${id}`).is(":checked")){
        val = 1;
      }

      $.ajax({
        url : url,
        method : 'POST',
        data : {
          "_token": "{{ csrf_token() }}",
          "id": id,
          "status": val
        },
        beforeSend:function(jqXHR,settings){

        },
        success:function(e){
          // console.log(e)
          // if(e=="OK"){
          //   $("#no"+no).attr("class","btn btn-sm btn-success");
          // }
        },
        error:function(x){
          // console.log(x)
          // $("#no"+no).attr("class","btn btn-sm btn-warning");
        }
      })
      return false;
    }
  </script>

  @endsection