<div class="col-sm-4 table-responsive">
  <p class="text-center" data-toggle="collapse" href="#collapseExample7" role="button" aria-expanded="true"
    aria-controls="collapseExample7" style="font-size:20px; border:1px solid black;"><b> + Minggu </b></p>
  <div class="collapse" id="collapseExample7">
    <div class="card card-body">
      <table id="barberman-datatables" class="table table-responsive table-striped table-bordered table-hover"
        width="100%" style="text-align: center;">
        <thead>
          <tr>
            <th>
              <center>No.</center>
            </th>
            <th>
              <center>Time</center>
            </th>
            <th>
              <center>Aksi</center>
            </th>
          </tr>
        </thead>
        <tbody>
          @foreach ($dataScheduleDaySeven as $item)
          <tr>
            <td>
              <center>{{$loop->iteration}}.</center>
            </td>
            <td>
              <center>{{$item->time->time}}</center>
            </td>
            <td>
              <label class="switch">
                <input type="checkbox" name="status" id="status{{$item->id}}" value="1" @if($item->status) checked @endif
                onclick="update({{$item->id}})">
                <span class="slider round"></span>
              </label>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>