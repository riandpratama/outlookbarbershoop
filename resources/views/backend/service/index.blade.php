@extends('adminlte::page')

@section('title', 'Service')

@section('content_header')
<h1>Service</h1>
@stop

@section('content')
<div class="row" style="padding-top: 20px;">
  <div class="col-md-12">
    {{-- <button data-title="Tambah" data-toggle="modal" data-target="#tambah" onclick="tambah(this)"
      class="btn btn-xs btn-info" style="color:black;"><i class="fa fa-plus"> Tambah Data Service</i>
    </button> --}}
    <hr>
    <div class="row">

      <div class="col-sm-12 table-responsive">
        <table id="barberman-datatables" class="table table-responsive table-striped table-bordered table-hover"
          width="100%" style="text-align: center;">
          <thead>
            <tr>
              <th>
                <center>No.</center>
              </th>
              <th>
                <center>Service Name</center>
              </th>
              <th>
                <center>Type</center>
              </th>
              <th>
                <center>Price</center>
              </th>
              <th>
                <center>Status</center>
              </th>
              <th>
                <center>Aksi</center>
              </th>
            </tr>
          </thead>
          <tbody>
            @foreach ($services as $item)
            <tr>
              <td>
                <center>{{$loop->iteration}}</center>
              </td>
              <td>
                <center>{{$item->name}}</center>
              </td>
              <td>
                <center>
                  @if($item->type == 1) Haircut @else Treatment @endif
                </center>
              </td>
              <td>
                <center>
                  @if($item->price == 0) *Ketetapan Harga berdasarkan barberman @else Start from Rp. {{ number_format($item->price, 2)}} @endif
                </center>
              </td>
              <td>
                <center>
                  @if($item->status == 1) Aktif @else Tidak Aktif @endif
                </center>
              </td>
              <td>
                <center>
                  <button data-title="Edit" data-toggle="modal" data-target="#edit" data-item="{{json_encode($item)}}"
                    onclick="edit(this)" class="btn btn-xs btn-info"><i class="fa fa-edit"></i>
                  </button>
                </center>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>

    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4><i class="fa fa-plus"></i> Data Service</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
          <form id="formEdit" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              <label for="name">Nama Service</label>
              <input id="id" type="hidden" name="id">
              <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
              @if ($errors->has('name'))
              <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
              <label for="type">Type</label>
              <select id="type" class="form-control" name="type">
                <option value="1">Haircut</option>
                <option value="2">Treatment</option>
              </select>
              @if ($errors->has('type'))
              <span class="help-block">
                <strong>{{ $errors->first('type') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
              <label for="status">Status</label>
              <select id="status" class="form-control" name="status">
                <option value="0">Tidak Aktif</option>
                <option value="1">Aktif</option>
              </select>
              @if ($errors->has('status'))
              <span class="help-block">
                <strong>{{ $errors->first('status') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
              <label for="price">Price</label>
              <input id="price" type="text" class="form-control" name="price" value="{{ old('price') }}">
              @if ($errors->has('price'))
              <span class="help-block">
                <strong>{{ $errors->first('price') }}</strong>
              </span>
              @endif
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-primary"
                onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span
                  class="glyphicon glyphicon-save"></span> Simpan</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"
                  aria-hidden="true"></span> Batal</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Tambah-->
  <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambah" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4><i class="fa fa-plus"></i> Tambah Service</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
          <form id="formTambah" method="POST" enctype="multipart/form-data" action="{{route('services.store')}}">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              <label for="name">Nama Service</label>
              <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
              @if ($errors->has('name'))
              <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
              <label for="type">Type Service</label>
              <select name="type" class="form-control">
                <option value="1">Haircut</option>
                <option value="2">Treatment</option>
              </select>
              @if ($errors->has('type'))
              <span class="help-block">
                <strong>{{ $errors->first('type') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
              <label for="price">Price</label>
              <input id="price" type="text" class="form-control" name="price" value="{{ old('price') }}">
              <i style="color:red;">*Input harga ketika memilih type Treatment</i>
              @if ($errors->has('price'))
              <span class="help-block">
                <strong>{{ $errors->first('price') }}</strong>
              </span>
              @endif
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-primary" onclick="return confirm('Anda yakin ingin menyelesaikan?')">
                <span class="glyphicon glyphicon-save"></span> Simpan
              </button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">
                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

</div>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
<script type="text/javascript">
  $(function () {
        $('#barberman-datatables').DataTable();
    });
</script>
<script type="text/javascript">
  edit = (button) => {
      var item = $(button).data('item');

      $('form#formEdit').attr('action','{{ url("backend/services") }}/'+item.id+'/update');
      $('#formEdit .form-group #id').val(item.id);
      $('#formEdit .form-group #name').val(item.name);
      $('#formEdit .form-group #type').val(item.type);
      $('#formEdit .form-group #status').val(item.status);
      $('#formEdit .form-group #price').val(item.price);
  }
</script>

@endsection