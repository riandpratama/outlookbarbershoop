@extends('adminlte::page')

@section('title', 'Booking')

@section('content_header')
<h1>Booking</h1>
@stop

@section('content')
<div class="row" style="padding-top: 20px;">
  <div class="col-md-12">
    <div class="row">

      <div class="col-sm-12 table-responsive">
        <table id="barberman-datatables" class="table table-responsive table-striped table-bordered table-hover"
          width="100%" style="text-align: center;">
          <thead>
            <tr>
              <th>
                <center>No.</center>
              </th>
              <th>
                <center>Customer</center>
              </th>
              <th>
                <center>Barberman</center>
              </th>
              <th>
                <center>Lokasi</center>
              </th>
              <th>
                <center>Tanggal Input Booking</center>
              </th>
              <th>
                <center>Tanggal & Waktu Booking</center>
              </th>
              <th>
                <center>Status</center>
              </th>
              <th>
                <center>Aksi</center>
              </th>
            </tr>
          </thead>
          <tbody>
            @foreach ($data as $item)
            <tr>
              <td>
                <center>{{$loop->iteration}}</center>
              </td>
              <td>
                <center>{{$item->customer->name}}</center>
              </td>
              <td>
                <center>{{$item->schedule->barberman->name}}</center>
              </td>
              <td>
                <center>{{$item->schedule->barberman->location->name}}</center>
              </td>
              <td>
                <center>{{date('d/m/y', strtotime($item->created_at))}}</center>
              </td>
              <td>
                <center>{{date('d/m/y', strtotime($item->date))}} - {{$item->schedule->time->time}}</center>
              </td>
              <td>
                <center>
                  @if ($item->status == 1) Hadir @else Tidak Hadir @endif
                </center>
              </td>
              <td>
                <center>
                  <button data-title="Edit" data-toggle="modal" data-target="#edit" data-item="{{json_encode($item)}}"
                    onclick="edit(this)" class="btn btn-xs btn-info"><i class="fa fa-edit"></i>
                  </button>
                </center>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>

    </div>
  </div>

  <!-- Modal Edit -->
  <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4><i class="fa fa-plus"></i> Status Kehadiran Customer</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
          <form id="formEdit" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
              <label for="status">Status Kehadiran</label>
              <input id="id" type="hidden" name="id">
              <select id="status" class="form-control" name="status">
                <option value="0">Tidak Hadir</option>
                <option value="1">Hadir</option>
              </select>
              @if ($errors->has('status'))
              <span class="help-block">
                <strong>{{ $errors->first('status') }}</strong>
              </span>
              @endif
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-primary"
                onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span
                  class="glyphicon glyphicon-save"></span> Simpan</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"
                  aria-hidden="true"></span> Batal</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
  <script type="text/javascript">
    $(function () {
        $('#barberman-datatables').DataTable({
          "lengthMenu": [[50, 100, 500, -1], [50, 100, 500, "All"]]
        });
    });
  </script>

  <script type="text/javascript">
    edit = (button) => {
        var item = $(button).data('item');

        $('form#formEdit').attr('action','{{ url("backend/booking") }}/'+item.id+'/update');
        $('#formEdit .form-group #id').val(item.id);
        $('#formEdit .form-group #name').val(item.name);
        $('#formEdit .form-group #price').val(item.price);
        $('#formEdit .form-group #status').val(item.status);
        $('#formEdit .form-group #location_id').val(item.location_id);
    }
  </script>

  @endsection