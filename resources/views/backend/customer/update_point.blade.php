@extends('adminlte::page')

@section('title', 'Point Customer')

@section('content_header')
  <h1>Point Customer</h1>
@stop

@section('content')
<div class="row" style="padding-top: 20px;">
  <div class="col-md-12">
    <h4>Nama Customer: {{$data->name}}</h4>
    <h4>Alamat Customer: {{$data->address}}</h4>
    <h4>No Telphone Customer: {{$data->phone}}</h4>
    <h4>ID QRCODE: {{$data->qrcode}}</h4>
    <h4>Currently Point: <b>{{number_format($data->rupiah_currently)}}</b></h4>
    <hr>
    <div class="row">
      <div class="tab-content padding-10">
        <div class="col-sm-12 table-responsive">
          <table id="barberman-datatables" class="table table-responsive table-striped table-bordered table-hover"
            width="100%" style="text-align: center;">
            <thead>
              <tr>
                <th>
                  <center>No.</center>
                </th>
                <th>
                  <center>Date</center>
                </th>
                <th>
                  <center>Point</center>
                </th>
                <th>
                  <center>Aksi</center>
                </th>
              </tr>
            </thead>
            <tbody>
              @foreach ($data->coupon as $item)
              <tr>
                <td>{{$loop->iteration}}.</td>
                <td>{{date('d/m/Y H:i:s', strtotime($item->created_at))}}</td>
                <td>{{number_format($item->rupiah)}}</td>
                <td>
                  <form action="{{route('customer.updatePointStore', $item->id)}}" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-xs btn-danger" onclick="return confirm('Anda yakin ingin menghapus point?')"><i class="fa fa-trash"></i> Delete point</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection