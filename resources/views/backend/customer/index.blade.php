@extends('adminlte::page')

@section('title', 'Customer')

@section('content_header')
    <h1>Customer</h1>
@stop

@section('content')

	<div class="row" style="padding-top: 20px;">
        <div class="col-md-12">
        	<div class="row">
        		<form action="{{ route('customer.cetak') }}" method="get">
		        	<div class="col-md-2">
			        	<select name="awal" id="" class="form-control js-select2">
			        		@foreach($qrcode as $item)
			        		<option value="{{ $item->qrcode }}" >{{ $item->qrcode }}</option>
			        		@endforeach
			        	</select>
		        	</div>
		        	<div class="col-md-2">
			        	<select name="akhir" id="" class="form-control js-select2">
			        		@foreach($qrcode as $item)
			        		<option value="{{ $item->qrcode }}" >{{ $item->qrcode }}</option>
			        		@endforeach
			        	</select>
		        	</div>
		        	<div class="col-md-2">
		        		<button type="submit" class="btn btn-primary"><i class="fa fa-print"></i> Cetak</button>
		        	</div>
	        	</form>
        	</div>
        	<hr>
        	<ul class="nav nav-tabs bordered">
                <li class="active"><a href="#link_1" data-toggle="tab">Customer</a></li>
                <li><a href="#link_2" data-toggle="tab">Kartu Barcode</a></li>
            </ul><br><br>

	        <div class="tab-content padding-10">
            	<div class="tab-pane active" id="link_1">
	                <!-- link_1 -->
	            	<div class="col-sm-12 table-responsive">
		                <table id="customers-datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
			                <thead>
			                    <tr>
			                        <th>
			                            <center>No.</center>
			                        </th>
			                        <th>
			                            <center>QRCode</center>
			                        </th>
			                        <th>
			                            <center>ID QRCode</center>
			                        </th>
			                        <th>
			                            <center>Nama</center>
			                        </th>
			                        <th>
			                            <center>Alamat</center>
			                        </th>
			                        <th>
			                            <center>Phone</center>
			                        </th>
			                        <th>
			                            <center>Tanggal Lahir</center>
			                        </th>
			                        <th>
			                            <center>Total Scan</center>
			                        </th>
			                        <th>
			                            <center>Total Penukaran</center>
			                        </th>
															<th>
																<center>Status Booking</center>
															</th>
			                        <th>
			                            <center>Edit</center>
			                        </th>
			                        <th>
			                            <center>Delete</center>
			                        </th>
															<th>
																<center>Update Point</center>
														</th>
			                    </tr>
			                </thead>
	            		</table>
	            	</div>
	            </div>

	            <div class="tab-pane" id="link_2">
	            	<div class="col-sm-12">
	            	<a href="{{ route('generate.success') }}" target="_blank" class="btn btn-primary">Klik Generate QRCode</a>
	            	</div>
	            	<hr>
	            	<hr>
		        	<div class="col-sm-12 table-responsive">
		                <table id="qrcode-datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
			                <thead>
			                    <tr>
			                        <th>
			                            <center>No.</center>
			                        </th>
			                        <th>
			                            <center>QRCode</center>
			                        </th>
			                        <th>
			                            <center>ID QRCode</center>
			                        </th>
			                        <th>
			                            <center>Tanggal Dibuat</center>
			                        </th>
			                        <th>
			                            <center>Tambahkan Customer</center>
			                        </th>
			                    </tr>
			                </thead>
	            		</table>
	            	</div>
		        </div>
	        </div>
        </div>
    </div>

    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered">
	    	<div class="modal-content">
	          	<div class="modal-header">
	          		<h4><i class="fa fa-plus"></i> Data Customer</h4>
		        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		      	</div>
	          	<div class="modal-body">
	          		<form id="formEdit" method="POST" enctype="multipart/form-data">
	          			{{ csrf_field() }}
	                	<div class="form-group{{ $errors->has('qrcode') ? ' has-error' : '' }}">
		                    <label for="qrcode">QrCode</label>
		                    <input id="qrcode" type="text" class="form-control" name="qrcode" value="{{ old('qrcode') }}" disabled="">
		                    @if ($errors->has('qrcode'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('qrcode') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
		                    <label for="name">Nama Customer</label>
		                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
		                    @if ($errors->has('name'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('name') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
		                    <label for="address">Alamat Customer</label>
		                    <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}">
		                    @if ($errors->has('address'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('address') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
		                    <label for="phone">No. Telpon/No. Handphone Customer</label>
		                    <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}">
		                    @if ($errors->has('phone'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('phone') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group{{ $errors->has('birth') ? ' has-error' : '' }}">
		                    <label for="birth">Tanggal Lahir</label>
		                    <input id="birth" type="date" class="form-control" name="birth" value="{{ old('birth') }}">
		                    @if ($errors->has('birth'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('birth') }}</strong>
		                        </span>
		                    @endif
		                </div>
										<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
											<label for="status">Status Booking</label>
											<select name="status" class="form-control">
												<option selected disabled>Pilih Status</option>
												<option value="0">Tidak Aktif</option>
												<option value="1">Aktif</option>
											</select>
											@if ($errors->has('status'))
													<span class="help-block">
															<strong>{{ $errors->first('status') }}</strong>
													</span>
											@endif
									</div>

		                <div class="modal-footer">
		        			<button type="submit" class="btn btn-primary" onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span class="glyphicon glyphicon-save"></span> Simpan</button>
		        			<button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
		      	  		</div>
	            	</form>
	        	</div>
		    </div>
		</div>
	</div>


<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
<script type="text/javascript">
    $(function () {
        $('#customers-datatables').DataTable({
            paging: true,
            searching: { "regex": true },
            lengthMenu: [ [25, 50, 100 -1], [25, 50, 100, "All"] ],
            pageLength: 25,
            serverSide: true,
            processing: true,
            ajax: '{{ route('getdataCustomer') }}',
            columns: [
                {data: 'DT_RowIndex'},
                {data: 'action'},
                {data: 'qrcode'},
                {data: 'name'},
                {data: 'address'},
                {data: 'phone'},
                {data: 'birth'},
                {data: 'coupon_status_zero_count'},
                {data: 'coupon_status_one_count'},
								{data: 'status'},
                {data: 'edit'},
                {data: 'delete'},
								{data: 'updatepoint'}
            ],
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $('#qrcode-datatables').DataTable({
            paging: true,
            searching: { "regex": true },
            lengthMenu: [ [10, 20, 25, 50, 100 -1], [10, 20, 25, 50, 100, "All"] ],
            pageLength: 10,
            serverSide: true,
            processing: true,
            ajax: '{{ route('getdataQrCode') }}',
            columns: [
                {data: 'DT_RowIndex'},
                {data: 'action'},
                {data: 'qrcode'},
                {data: 'created_at'},
                {data: 'edit'},
            ],
        });
    });
</script>

<script type="text/javascript">
	edit = (button) => {
        var item = $(button).data('item');
        console.log(item);
        $('form#formEdit').attr('action','{{ url("backend/customers") }}/'+item.qrcode+'/update');
        $('#formEdit .form-group #qrcode').val(item.qrcode);
        $('#formEdit .form-group #name').val(item.name);
        $('#formEdit .form-group #address').val(item.address);
        $('#formEdit .form-group #phone').val(item.phone);
        $('#formEdit .form-group #birth').val(item.birth);
				$('#formEdit .form-group #status').val(item.status);
    }
</script>

<script>
	$(document).ready(function() {
	    $('.js-select2').select2();
	});
</script>

<script>
	function hapus(obj) {
        var item = $(obj).data('item');
        Swal.fire({
            title: 'Apakah kamu yakin?',
            text: "Ingin Menghapus? ",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Hapus',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $.post("{{route('customer.delete')}}", {id: item.id,_token: "{{csrf_token()}}"},function(data){
                    var json = JSON.parse(data);
                    if (json.status = 'success') {
                        Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: 'Berhasil Menghapus',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        location.reload();
                    } else {
                        Swal.fire({
                            position: 'center',
                            type: 'error',
                            title: 'Gagal Menghapus',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        location.reload();
                    }
                });
            }
        });
    }
</script>

@stop