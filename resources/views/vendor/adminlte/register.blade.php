@extends('adminlte::master')

@section('adminlte_css')
    @yield('css')
@stop

@section('body_class', 'register-page')

@section('body')
    <div class="register-box">

        <div class="register-logo">
            <b>404 Not Found</b>
        </div>

    </div><!-- /.register-box -->
@stop

@section('adminlte_js')
    @yield('js')
@stop
