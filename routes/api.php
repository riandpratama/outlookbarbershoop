<?php
Route::group(['namespace' => 'API'] ,function(){
	Route::group(['prefix' => 'auth'] ,function(){
		Route::post('login', 'AuthController@login');
		Route::post('register', 'AuthController@register');
	});
	// Route::group(['middleware' => 'auth.jwt'] ,function(){

		Route::get('profile', 'AuthController@getAuthenticatedUser');
		Route::get('logout', 'AuthController@logout');

		Route::group(['prefix' => 'coupon'] ,function(){
		    Route::get('', 'CouponController@coupon');
			Route::put('coupon-update', 'CouponController@couponUpdate');
			Route::get('show/{qrcode}', 'CouponController@show');
			Route::put('update/{qrcode}', 'CouponController@updateCoupon');
			Route::put('update-rupiah/{qrcode}', 'CouponController@updateCouponRupiah');
			Route::put('update-rupiah-v2/{qrcode}', 'CouponController@updateCouponRupiahVersion2');
			Route::put('exchange/{qrcode}', 'CouponController@exchangeCoupon');
			Route::put('exchange-rupiah/{qrcode}', 'CouponController@exchangeRupiah');
		});
		Route::group(['prefix' => 'customer'] ,function(){
			Route::get('/', 'CustomerController@index');
			Route::get('/reward', 'CustomerController@reward');
			Route::get('show/{qrcode}', 'CustomerController@show');
			Route::put('update/{qrcode}', 'CustomerController@update');
			Route::put('delete/{qrcode}', 'CustomerController@destroy');
		});
		Route::group(['prefix' => 'point'] ,function(){
			Route::get('/', 'PointController@index');
			Route::get('/rupiah', 'PointController@indexPointRupiah');
		});
	// });

	include(__DIR__ . '/V2/api.php');
});

