<?php

Route::group(['middleware' => 'auth.jwt'] ,function(){
  Route::group(['prefix' => 'v2'] ,function(){
    Route::group(['prefix' => 'customer', 'namespace' => 'V2'] ,function(){
      Route::get('/', 'CustomerController@index');
    });
  });
});