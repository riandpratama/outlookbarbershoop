<?php

Route::get('/migrate', function () {
	Artisan::call('migrate');
});

Route::get('/cache', function () {
	Artisan::call('config:cache');
});

Route::get('/seed', function () {
	Artisan::call('db:seed');
	return view('backend.customer.success');
})->name('generate.success');


/*Frontend*/
Route::group(['namespace' => 'Frontend'], function () {
	Route::get('/', 'MainController@index');
});

Auth::routes();

/*Backend*/
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['namespace' => 'Backend', 'prefix' => 'backend'], function () {
	Route::get('/customers', 'CustomerController@index')->name('customer.index');
	Route::post('/customers/{qrcode}/update', 'CustomerController@update')->name('customer.update');
	Route::get('/customers/cetak', 'CustomerController@cetak')->name('customer.cetak');
	Route::post('/customers/delete', 'CustomerController@delete')->name('customer.delete');
	Route::get('/customers/update_point/{id}', 'CustomerController@updatePoint')->name('customer.updatePoint');
	Route::post('/customers/update_point_store/{id}', 'CustomerController@updatePointStore')->name('customer.updatePointStore');

	Route::get('/barbermans', 'BarbermanController@index')->name('barberman.index');
	Route::post('/barbermans/store', 'BarbermanController@store')->name('barberman.store');
	Route::post('/barbermans/{id}/update', 'BarbermanController@update')->name('barberman.update');

	Route::get('/services', 'ServiceController@index')->name('services.index');
	Route::post('/services/store', 'ServiceController@store')->name('services.store');
	Route::post('/services/{id}/update', 'ServiceController@update')->name('service.update');

	Route::get('/times', 'TimeController@index')->name('times.index');
	Route::post('/times/store', 'TimeController@store')->name('times.store');
	Route::post('/times/{id}/update', 'TimeController@update')->name('times.update');

	Route::get('/schedules', 'ScheduleController@index')->name('schedule.index');
	Route::get('/schedules/{id}', 'ScheduleController@show')->name('schedule.show');
	Route::post('/schedules/store', 'ScheduleController@store')->name('schedule.store');
	Route::post('/schedules/{id}/update', 'ScheduleController@update')->name('schedule.update');

	Route::get('/bookings', 'BookingController@index')->name('booking.index');
	Route::post('/booking/{id}/update', 'BookingController@update')->name('booking.update');

	Route::get('/point', 'PointController@index')->name('point.index');
	Route::post('/point/{id}/update', 'PointController@update')->name('point.update');

	Route::post('/point/store', 'PointController@store')->name('point.store');
	Route::post('/point/{id}/update-select', 'PointController@updateSelect')->name('point.updateselect');
	Route::get('/point/{id}/delete', 'PointController@destroy')->name('point.destroy');

	Route::get('/description', 'DescriptionController@index')->name('description.index');
	Route::post('/description/{id}/update', 'DescriptionController@update')->name('description.update');
});

/*service*/
Route::get('/customer/get-data-customer', 'Backend\CustomerController@getDataCustomer')->name('getdataCustomer');
Route::get('/customer/get-data-qrcode', 'Backend\CustomerController@getDataQrCode')->name('getdataQrCode');
