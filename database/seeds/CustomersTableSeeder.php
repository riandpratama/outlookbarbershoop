<?php

// use SimpleSoftwareIO\QrCode\Generator as QrCode;

use Carbon\Carbon;
use App\Models\Customer;
use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // dd(base_path()."/public/images/");
        $lastIdQRCode = Customer::max('qrcode');
        // dd('qrcode/'.$lastIdQRCode.'.png');
    	for ($i=1; $i<=50; $i++) {
	        DB::table('customers')->insert([
	            'qrcode' => $lastIdQRCode+$i,
                'qrcode_image' => $lastIdQRCode+$i.".png",
                'created_at' => Carbon::now()
	        ]);
            // \QrCode::size(500)->format('png')->generate($lastIdQRCode+$i, public_path('images/', $lastIdQRCode + $i));
            \QrCode::format('png')
                    ->size(300)
                    ->backgroundColor(255, 255, 255)
                    ->generate(($lastIdQRCode+$i), base_path() . "/storage/app/public/qrcode/" . ($lastIdQRCode+$i) . '.png');
            // \QrCode::generate($lastIdQRCode+$i, '../public/images');
        }
    }
}
