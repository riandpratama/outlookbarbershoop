<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
  protected $table = 'bookings';

  protected $guarded = [];

  public function schedule()
  {
    return $this->belongsTo(Schedule::class)->withDefault();
  }

  public function customer()
  {
    return $this->belongsTo(Customer::class)->withDefault();
  }
}
