<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Barberman extends Model
{
  protected $table = 'barbermans';

  protected $guarded = [];

  public function location()
  {
    return $this->belongsTo(Location::class)->withDefault();
  }
}
