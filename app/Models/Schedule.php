<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
  protected $table = 'schedules';

  protected $guarded = [];

  public function time()
  {
    return $this->belongsTo(Time::class)->withDefault();
  }

  public function barberman()
  {
    return $this->belongsTo(Barberman::class)->withDefault();
  }

  public function location()
  {
    return $this->belongsTo(Location::class)->withDefault();
  }
}
