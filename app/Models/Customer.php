<?php

namespace App\Models;

use App\Models\Coupon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
	use SoftDeletes;

    protected $table = 'customers';

    protected $guarded = [];

    public function coupon() {
    	return $this->hasMany(Coupon::class, 'customer_id')->orderBy('id','desc');
    }

    public function couponStatusZero() {
    	return $this->hasMany(Coupon::class, 'customer_id')->orderBy('id','desc')->where('status_rupiah', 0);
    }

    public function couponStatusOne() {
    	return $this->hasMany(Coupon::class, 'customer_id')->orderBy('id','desc')->where('status_rupiah', 1);
    }

}
