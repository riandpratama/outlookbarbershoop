<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CouponChange extends Model
{	
    protected $table = 'coupon_changes';

    protected $guarded = [];
}
