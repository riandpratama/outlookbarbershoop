<?php

namespace App\Http\Controllers\Backend;

use App\Models\Booking;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Booking::orderBy('created_at', 'desc')->get();

        return view('backend.booking.index', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Booking::findOrFail($id);

        $customer = Customer::where('id', $data->customer_id)->first();

        $data->update([
            'status' => ($request->status == 1) ? 1 : 0,
        ]);

        if ($request->status == 0) {
            $customer->update([
                'status' => 0
            ]);
        }

        return redirect()->back();
    }
}
