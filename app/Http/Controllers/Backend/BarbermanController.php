<?php

namespace App\Http\Controllers\Backend;

use DB;
use File;
use App\Models\Barberman;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class BarbermanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['barbermans'] = DB::table('barbermans')
            ->select(
                'barbermans.id',
                'barbermans.name',
                'barbermans.email',
                'barbermans.status',
                'barbermans.discount',
                'barbermans.price',
                'barbermans.price_discount',
                'barbermans.service',
                'barbermans.location_id',
                'locations.name as location',
                'barbermans.available_gender',
                'photo'
            )
            ->join('locations', 'locations.id', '=', 'barbermans.location_id')
            ->orderBy('barbermans.id', 'asc')
            ->get();
        $data['locations'] =  DB::table('locations')->orderBy('created_at', 'asc')->get();

        return view('backend.barberman.index', $data);
    }

    /**
     * Store the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $price_discount = 0;

        if ($request->price) {
            $price_discount = $request->price * 15 / 100;
        }

        if ($request->hasFile('photo')) {
            $file = $request->file('photo')->store('uploads');
        } else {
            $file = 'uploads/default.png';
        }

        DB::table('barbermans')->insert([
            'name' => $request->name,
            'email' => $request->email,
            'status' => 1,
            'price' => ($request->price) ? $request->price : 0,
            'service' => $request->service,
            'location_id' => $request->location_id,
            'price_discount' => ($price_discount > 0) ? $request->price - $price_discount : 0,
            'photo' => $file,
            'available_gender' => $request->available_gender
        ]);

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Barberman::findOrFail($id);

        if ($request->hasFile('photo')) {
            $file = $request->file('photo')->store('uploads');
            if (asset('storage/' . $data->photo)) {
                Storage::delete($data->photo);
            }
        } else {
            $file = $data->photo;
        }

        $price_discount = 0;

        if ($request->price) {
            $price_discount = $request->price * $request->discount / 100;
        }

        $data->update([
            'name' => $request->name ? $request->name : $data->name,
            'email' => $request->email ? $request->email : $data->email,
            'status' => ($request->status == 1) ? 1 : 0,
            'price' => $request->price ? $request->price : $data->price,
            'discount' => $request->discount ? $request->discount : $data->discount,
            'price_discount' => ($price_discount > 0) ? $request->price - $price_discount : 0,
            'location_id' => $request->location_id ? $request->location_id : $data->location_id,
            'service' => $request->service ? $request->service : $data->service,
            'photo' => $file,
            'available_gender' => $request->available_gender
        ]);

        return redirect()->route('barberman.index');
    }
}
