<?php

namespace App\Http\Controllers\Backend;

use DB;
use PDF;
use File;
use Storage;
use DataTables;
use App\Models\Coupon;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['qrcode'] = DB::table('customers')
            ->select(
                'customers.name',
                'customers.qrcode',
                'customers.deleted_at'
            )
            ->whereNull('name')
            ->whereNull('deleted_at')
            ->orderBy('qrcode', 'asc')
            ->get();

        return view('backend.customer.index', $data);
    }

    public function getDataCustomer()
    {
        // $all = DB::table('customers')
        //     ->select(
        //         'customers.id',
        //         'customers.name',
        //         'customers.address',
        //         'customers.phone',
        //         'customers.birth',
        //         'customers.qrcode',
        //         'customers.qrcode_image',
        //         'customers.cuopon_currently',
        //         'customers.cuopon_exchange',
        //         'customers.deleted_at',
        //         'customers.status',
        //         DB::raw('count(coupons.customer_id) as user_count')
        //     )
        //     ->whereNotNull('customers.name')
        //     ->whereNull('customers.deleted_at')
        //     ->join('coupons', 'coupons.customer_id', 'customers.id')
        //     ->orderBy('qrcode', 'asc')
        //     ->limit(5)->get();
        $all = Customer::withCount('couponStatusZero')->withCount('couponStatusOne')
        ->whereNotNull('customers.name')
        ->whereNull('customers.deleted_at')
        ->get();
        
        $url = asset('storage/qrcode');

        return $allDataTables = Datatables::of($all)->addIndexColumn()
            ->addColumn('action', function ($item) use ($url) {
                return '<img src=' . "$url" . '/' . "$item->qrcode_image" . ' width="100"> ';
            })
            ->addColumn('birth', function ($item) {
                return date('d/m/Y', strtotime($item->birth));
            })
            ->addColumn('status', function ($item) {
                return ($item->status == 0) ? 'Tidak Aktif' : 'Aktif';
            })
            ->addColumn('delete', function ($val) {
                return "<button
                                                data-item='" . json_encode($val) . "' onclick='hapus(this)'
                                                class='btn btn-xs btn-danger'><i class='fa fa-trash'></i>
                                            </button>";
            })
            ->addColumn('edit', function ($val) {
                return "<button
                                                data-title='Edit' data-toggle='modal' data-target='#edit'
                                                data-item='" . json_encode($val) . "' onclick='edit(this)'
                                                class='btn btn-xs btn-info'><i class='fa fa-edit'></i>
                                            </button>";
            })->addColumn('updatepoint', function ($val) {
                return "<a
                                                href='" . route('customer.updatePoint', $val->id) ."'
                                                class='btn btn-xs btn-success'><i class='fa fa-database'></i>
                                            </a>";
            })->rawColumns(['action', 'edit', 'delete', 'updatepoint'])->make(true);
    }
    public function getDataQrCode()
    {
        $all = DB::table('customers')
            ->select(
                'customers.id',
                'customers.name',
                'customers.address',
                'customers.phone',
                'customers.birth',
                'customers.qrcode',
                'customers.qrcode_image',
                'customers.created_at',
                'customers.deleted_at'
            )
            ->whereNull('name')
            ->whereNull('deleted_at')
            ->orderBy('qrcode', 'asc')
            ->get();

        $url = asset('storage/qrcode/');

        return $allDataTables = Datatables::of($all)->addIndexColumn()
            ->addColumn('action', function ($item) use ($url) {
                return '<img src=' . "$url" . '/' . "$item->qrcode_image" . ' width="100"> ';
            })
            ->addColumn('edit', function ($val) {
                return "<button
                                                data-title='Edit' data-toggle='modal' data-target='#edit'
                                                data-item='" . json_encode($val) . "' onclick='edit(this)'
                                                class='btn btn-xs btn-primary'><i class='fa fa-plus'></i>
                                            </button>";
            })
            ->addColumn('created_at', function ($item) {
                return date('d/m/Y', strtotime($item->created_at));
            })->rawColumns(['action', 'edit'])->make(true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $qrcode)
    {
        $data = Customer::where('qrcode', $qrcode)->first();

        $data->update([
            'name' => $request->name ? $request->name : $data->name,
            'address' => $request->address ? $request->address : $data->address,
            'phone' => $request->phone ? $request->phone : $data->phone,
            'birth' => $request->birth ? $request->birth : $data->birth,
            'status' => ($request->status == 1) ? 1 : 0,
        ]);

        return redirect()->route('customer.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $msg = [];
        try {
            $customer = Customer::where('id', $request->id)->first();
            if (File::isFile('storage/qrcode/' . $customer->qrcode_image)) {
                File::delete('storage/qrcode/' . $customer->qrcode_image);
            }
            $customer->delete();
            $msg = ["status" => "success"];
        } catch (Exception $e) {
            $msg = ["status" => "error"];
        }

        echo json_encode($msg);
    }

    public function cetak(Request $request)
    {
        $customer = Customer::whereBetween('qrcode', [$request->awal, $request->akhir])->orderBy('qrcode', 'asc')->get();

        $pdf = PDF::loadView('backend.customer.cetak', compact('customer'))->setPaper('A4', 'portrait');
        return $pdf->stream('cetak.pdf', 'customer');
    }

    public function updatePoint($id)
    {
        $data = Customer::findOrFail($id);

        return view('backend.customer.update_point', compact('data'));
    }

    public function updatePointStore($id)
    {
        $dataCoupon = Coupon::findOrFail($id);
        $dataCustomer = Customer::findOrFail($dataCoupon->customer_id);

        if ($dataCustomer->rupiah_currently >= $dataCoupon->rupiah) {

            $dataCoupon->delete();

            $dataCustomer->update(['rupiah_currently' => ($dataCustomer->rupiah_currently - $dataCoupon->rupiah)]);
        }

        return redirect()->back();
    }
}
