<?php

namespace App\Http\Controllers\Backend;

use DB;
use PDF;
use File;
use Storage;
use DataTables;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['times'] = DB::table('times')
            ->select(
                'times.id',
                'times.time'
            )
            ->orderBy('id', 'asc')
            ->get();

        return view('backend.times.index', $data);
    }

    /**
     * Store the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('times')->insert([
            'time' => $request->time,
        ]);

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = DB::table('times')->where('id', $id);

        $data->update([
            'time' => $request->time ? $request->time : $data->time,
        ]);

        return redirect()->route('times.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $msg = [];
        try {
            $customer = Customer::where('id', $request->id)->first();
            if (File::isFile('storage/qrcode/' . $customer->qrcode_image)) {
                File::delete('storage/qrcode/' . $customer->qrcode_image);
            }
            $customer->delete();
            $msg = ["status" => "success"];
        } catch (Exception $e) {
            $msg = ["status" => "error"];
        }

        echo json_encode($msg);
    }
}
