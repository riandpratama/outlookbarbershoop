<?php

namespace App\Http\Controllers\Backend;

use DB;
use File;
use Storage;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['services'] = DB::table('services')
            ->select(
                'services.id',
                'services.name',
                'services.type',
                'services.status',
                'services.price'
            )
            ->orderBy('created_at', 'asc')
            ->get();

        return view('backend.service.index', $data);
    }

    /**
     * Store the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Service::create([
            'name' => $request->name,
            'type' => $request->type,
            'status' => 1,
            'service' => $request->service,
        ]);

        return redirect()->route('services.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Service::findOrFail($id);

        $data->update([
            'name' => $request->name ? $request->name : $data->name,
            'type' => $request->type ? $request->type : $data->type,
            'status' => ($request->status == 1) ? 1 : 0,
            'price' => $request->price ? $request->price : $data->price,
        ]);

        return redirect()->route('services.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $msg = [];
        try {
            $customer = Customer::where('id', $request->id)->first();
            if (File::isFile('storage/qrcode/' . $customer->qrcode_image)) {
                File::delete('storage/qrcode/' . $customer->qrcode_image);
            }
            $customer->delete();
            $msg = ["status" => "success"];
        } catch (Exception $e) {
            $msg = ["status" => "error"];
        }

        echo json_encode($msg);
    }
}
