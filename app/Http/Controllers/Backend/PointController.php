<?php

namespace App\Http\Controllers\Backend;

use DB;
use App\Models\Point;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['point'] = DB::table('point')->first();
        $data['pointSelect'] = Point::orderBy('id', 'asc')->get()->slice(1);

        return view('backend.point.index', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Point::findOrFail($id);

        $data->update([
            'point' => $request->point ? $request->point : $data->point,
        ]);

        return redirect()->back();
    }

    /** List inputan select rupiah penukaran potong*/
    /**
     * Create the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Point::create([
            'point' => $request->point,
        ]);

        return redirect()->back();
    }
    /**
     * Delete the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Point::findOrFail($id);

        $data->delete();

        return redirect()->back();
    }
}