<?php

namespace App\Http\Controllers\Backend;

use DB;
use PDF;
use File;
use Storage;
use DataTables;
use App\Models\Schedule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['barbermans_lokasi_1'] = DB::table('barbermans')
            ->select(
                'barbermans.id',
                'barbermans.name',
                'barbermans.status',
                'barbermans.price',
                'barbermans.service',
                'barbermans.location_id',
                'locations.name as location'
            )
            ->join('locations', 'locations.id', '=', 'barbermans.location_id')
            ->where('location_id', 1)
            ->orderBy('barbermans.created_at', 'asc')
            ->get();

        $data['barbermans_lokasi_2'] = DB::table('barbermans')
            ->select(
                'barbermans.id',
                'barbermans.name',
                'barbermans.status',
                'barbermans.price',
                'barbermans.service',
                'barbermans.location_id',
                'locations.name as location'
            )
            ->join('locations', 'locations.id', '=', 'barbermans.location_id')
            ->where('location_id', 2)
            ->orderBy('barbermans.created_at', 'asc')
            ->get();

        $data['barbermans_all'] = DB::table('barbermans')
            ->select(
                'barbermans.id',
                'barbermans.name',
                'barbermans.status',
                'barbermans.price',
                'barbermans.location_id',
                'locations.name as location'
            )
            ->join('locations', 'locations.id', '=', 'barbermans.location_id')
            ->orderBy('barbermans.created_at', 'asc')
            ->get();

        return view('backend.schedule.index', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $data['data'] = DB::table('barbermans')->where('id', $id)->first();

        $data['dataScheduleDayOne'] = Schedule::where('barberman_id', $data['data']->id)->where('day_id', 1)->get();
        $data['dataScheduleDayTwo'] = Schedule::where('barberman_id', $data['data']->id)->where('day_id', 2)->get();
        $data['dataScheduleDayThree'] = Schedule::where('barberman_id', $data['data']->id)->where('day_id', 3)->get();
        $data['dataScheduleDayFour'] = Schedule::where('barberman_id', $data['data']->id)->where('day_id', 4)->get();
        $data['dataScheduleDayFive'] = Schedule::where('barberman_id', $data['data']->id)->where('day_id', 5)->get();
        $data['dataScheduleDaySix'] = Schedule::where('barberman_id', $data['data']->id)->where('day_id', 6)->get();
        $data['dataScheduleDaySeven'] = Schedule::where('barberman_id', $data['data']->id)->where('day_id', 7)->get();

        return view('backend.schedule.show', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Schedule::where('barberman_id', $request->barberman_id)->where('day_id', $request->day_id)->get()->count();
        $timeCount = DB::table('times')->get()->count();

        if ($data > 0) {
            return redirect()->back();
        }

        for ($time = 1; $time <= $timeCount; $time++) {
            Schedule::create([
                'barberman_id' => $request->barberman_id,
                'day_id' => $request->day_id,
                'time_id' => $time,
                'status' => 0
            ]);
        }

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Schedule::findOrFail($id);

        $data->update([
            'status' => ($request->status == 1) ? 1 : 0,
        ]);

        return "Ok";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $msg = [];
        try {
            $customer = Customer::where('id', $request->id)->first();
            if (File::isFile('storage/qrcode/' . $customer->qrcode_image)) {
                File::delete('storage/qrcode/' . $customer->qrcode_image);
            }
            $customer->delete();
            $msg = ["status" => "success"];
        } catch (Exception $e) {
            $msg = ["status" => "error"];
        }

        echo json_encode($msg);
    }
}
