<?php

namespace App\Http\Controllers\Backend;

use DB;
use App\Models\Description;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['description'] = DB::table('descriptions')->first();

        return view('backend.description.index', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Description::findOrFail($id);

        $data->update([
            'top_desc' => $request->top_desc ? $request->top_desc : $data->top_desc,
            'middle_desc' => $request->middle_desc ? $request->middle_desc : $data->middle_desc,
            'bottom_desc' => $request->bottom_desc ? $request->bottom_desc : $data->bottom_desc,
        ]);

        return redirect()->back();
    }
}
