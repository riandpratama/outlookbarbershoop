<?php

namespace App\Http\Controllers\API\V2;

use DB;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Customer::whereNotNull('name')
            ->with('coupon')
            ->orderBy('updated_at', 'desc')
            ->when(
                isset($request['search']) && !is_null($request['search']),
                function ($q) use ($request) {
                    $q->where(function ($query) use ($request) {
                        $query->orWhere('name', 'like', "%{$request['search']}%")
                            ->orWhere('qrcode', 'like', "%{$request['search']}%");
                    });
                }
            )
            ->paginate(15);

        $point = DB::table('point')->select('point')->first()->point;
        foreach ($data as $item){
            $item['point'] = $point;
        }

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, Customer is nothing'
            ], 400);
        } else {
            return response()->json([
                    'success' => true,
                    'message' => $data,
                ], 200);
        }
    }
}