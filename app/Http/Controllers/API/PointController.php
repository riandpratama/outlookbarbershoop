<?php

namespace App\Http\Controllers\API;

use DB;
use App\Models\Point;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPointRupiah()
    {
        $temp = [];
        $data = Point::get()->slice(1);
        // $subset = $data->map(function ($item) {
        //     return collect($item->toArray())
        //         ->only(['id', 'point'])
        //         ->all();
        // });
        // dd($subset);
        foreach($data as $item) {
            $temp[] = $item->point;
        }
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, point is nothing'
            ], 400);
        } else {
            return response()->json([
                    'success' => true,
                    'message' => $temp
                ], 200);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('point')->first();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, point is nothing'
            ], 400);
        } else {
            return response()->json([
                    'success' => true,
                    'message' => $data
                ], 200);
        }
    }
}