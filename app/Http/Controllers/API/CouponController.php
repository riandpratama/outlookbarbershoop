<?php

namespace App\Http\Controllers\API;

use DB;
use App\Models\Coupon;
use App\Models\Customer;
use App\Models\CouponChange;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CouponController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $qrcode
     * @return \Illuminate\Http\Response
     */
    public function show($qrcode)
    {
        $data = Customer::where('qrcode', $qrcode)->first();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, Customer cannot be found'
            ], 400);
        } else {
            return response()->json([
                    'success' => true,
                    'message' => $data
                ], 200);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateCoupon(Request $request, $qrcode)
    {
        $data = Customer::where('qrcode', $qrcode)->first();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, Customer cannot be found'
            ], 400);
        } else {
            Coupon::create([
                'user_id' => 1,
                'customer_id' => $data->id,
            ]);

            $plus = $data->cuopon_currently+1;

            $data->update([
                'cuopon_currently' => $plus,
            ]);

            return response()->json([
                    'success' => true,
                    'message' => $data
                ], 201);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function exchangeCoupon(Request $request, $qrcode)
    {
        $data = Customer::where('qrcode', $qrcode)->first();
        $dataCoupon = DB::table('coupon_changes')->first()->coupon;
        $dataCouponMinus = DB::table('coupon_changes')->first()->coupon-1;

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, Customer cannot be found'
            ], 400);
        } elseif ($data->cuopon_currently >= $dataCoupon) {

            $minus = $data->cuopon_currently-$dataCoupon;
            $plus = $data->cuopon_exchange+1;

            $data->update([
                'cuopon_currently' => $minus,
                'cuopon_exchange' => $plus,
            ]);

            Coupon::create([
                'user_id' => 1,
                'status' => 1,
                'customer_id' => $data->id,
            ]);

            return response()->json([
                    'success' => true,
                    'message' => $data
                ], 200);
        } elseif ($data->cuopon_currently >= $dataCouponMinus) {

            $minus = $data->cuopon_currently-$dataCouponMinus;
            $plus = $data->cuopon_exchange+1;

            $data->update([
                'cuopon_currently' => $minus,
                'cuopon_exchange' => $plus,
            ]);

            Coupon::create([
                'user_id' => 1,
                'customer_id' => $data->id,
            ]);

            Coupon::create([
                'user_id' => 1,
                'status' => 1,
                'customer_id' => $data->id,
            ]);

            return response()->json([
                    'success' => true,
                    'message' => $data
                ], 200);
        } else {
            return response()->json([
                    'success' => false,
                    'message' => 'Sorry, Coupon cannot be exchange'
                ], 200);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateCouponRupiah(Request $request, $qrcode)
    {
        $data = Customer::where('qrcode', $qrcode)->first();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, Customer cannot be found'
            ], 400);
        } else {
            Coupon::create([
                'user_id' => 1,
                'customer_id' => $data->id,
                'rupiah' => $request->rupiah
            ]);

            // $plus = $data->cuopon_currently+1;
        //    $tes = (string) $request->rupiah;
        //    dd(typeOf())
            dd(str_replace('.', '', $request->rupiah));
            $rupiahcurrent = $data->rupiah_currently+$request->rupiah;

            $data->update([
                // 'cuopon_currently' => $plus,
                'rupiah_currently' => $rupiahcurrent
            ]);

            return response()->json([
                    'success' => true,
                    'message' => $data
                ], 201);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateCouponRupiahVersion2(Request $request, $qrcode)
    {
        $data = Customer::where('qrcode', $qrcode)->first();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, Customer cannot be found'
            ], 400);
        } else {
            Coupon::create([
                'user_id' => 1,
                'customer_id' => $data->id,
                'rupiah' => str_replace('.', '', $request->rupiah)
            ]);

            $newReqRupiah = str_replace('.', '', $request->rupiah);
            $rupiahcurrent = $data->rupiah_currently+$newReqRupiah;

            $data->update([
                // 'cuopon_currently' => $plus,
                'rupiah_currently' => $rupiahcurrent
            ]);

            return response()->json([
                    'success' => true,
                    'message' => $data
                ], 201);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function exchangeRupiah(Request $request, $qrcode)
    {
        $data = Customer::where('qrcode', $qrcode)->first();
        $dataRupiah = DB::table('point')->first()->point;

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, Customer cannot be found'
            ], 400);
        } elseif ($data->rupiah_currently >= $dataRupiah) {
            $minus = $data->rupiah_currently-$dataRupiah;
            $plus = $data->rupiah_exchange+1;

            $data->update([
                'rupiah_currently' => $minus,
                'rupiah_exchange' => $plus,
            ]);

            Coupon::create([
                'user_id' => 1,
                'status_rupiah' => 1,
                'rupiah' => $dataRupiah,
                'customer_id' => $data->id,
            ]);

            return response()->json([
                    'success' => true,
                    'message' => $data
                ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, Saldo cannot enough'
            ], 400);
        }
    }

    /**
     * Update the specified resource coupon.
     *
     * @return \Illuminate\Http\Response
     */
    public function coupon()
    {
        $data = CouponChange::where('id', 1)->first();

        return response()->json([
                'success'   => true,
                'message'   => $data
            ], 200);
    }

    /**
     * Update the specified resource coupon.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function couponUpdate(Request $request)
    {
        $data = CouponChange::where('id', 1)->first();

        $data->update([
            'coupon' => $request->coupon ? $request->coupon : $data->coupon
        ]);

        return response()->json([
                'success'   => true,
                'message'   => $data
            ], 200);
    }
}
