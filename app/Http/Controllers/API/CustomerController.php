<?php

namespace App\Http\Controllers\API;

use DB;
use App\Models\Coupon;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Customer::whereNotNull('name')->with('coupon')->orderBy('updated_at', 'desc')->get();
        $point = DB::table('point')->select('point')->first()->point;
        foreach ($data as $item){
            $item['point'] = $point;
        }

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, Customer is nothing'
            ], 400);
        } else {
            return response()->json([
                    'success' => true,
                    'message' => $data,
                ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $qrcode
     * @return \Illuminate\Http\Response
     */
    public function show($qrcode)
    {
        $data = Customer::where('qrcode', $qrcode)->first();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, Customer cannot be found'
            ], 400);
        } else {
            return response()->json([
                    'success' => true,
                    'message' => $data
                ], 200);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $qrcode)
    {
        $data = Customer::where('qrcode', $qrcode)->first();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, Customer cannot be found'
            ], 400);
        } else {

            $data->update([
                'name' => $request->name ? $request->name : $data->name,
                'address' => $request->address ? $request->address : $data->address,
                'phone' => $request->phone ? $request->phone : $data->phone,
                'birth' => $request->birth ? $request->birth : $data->birth,
            ]);

            return response()->json([
                    'success' => true,
                    'message' => $data
                ], 201);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reward()
    {
        $data = Customer::join('coupons', 'customers.id', 'coupons.customer_id')
                ->selectRaw('count(*) as value, name')
                ->groupBy('customer_id', 'name')
                ->where('coupons.status', 0)
                ->whereNotNull('name')
                ->whereNull('customers.deleted_at')
                ->orderBy('value', 'desc')->limit(3)->get();

        $year = \Carbon\Carbon::now()->format('Y');
        $month = \Carbon\Carbon::now()->format('m');
        $day = \Carbon\Carbon::now()->format('d');

        $data = Customer::whereMonth('birth', '=', $month)
                ->whereDay('birth', '=', $day)
                ->get();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, Customer is nothing'
            ], 400);
        } else {
            return response()->json([
                    'success' => true,
                    'message' => $data
                ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $qrcode
     * @return \Illuminate\Http\Response
     */
    public function destroy($qrcode)
    {
        $data = Customer::where('qrcode', $qrcode)->first();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, data cannot be found'
            ], 400);
        }

        $data->update([
            'name'              => NULL,
            'address'           => NULL,
            'phone'             => NULL,
            'birth'             => NULL,
            'birth'             => NULL,
            'cuopon_currently'  => 0,
            'cuopon_exchange'   => 0,
        ]);

        $data->coupon()->delete();

        return response()->json([
            'success' => true,
            'message' => 'Customer has been deleted'
        ]);
    }
}
